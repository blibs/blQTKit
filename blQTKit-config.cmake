find_package(blc_image REQUIRED)

find_path(BL_QTKIT_INCLUDE_DIR blQTKit.h PATH_SUFFIXES blQTKit)
find_library(BL_QTKIT_LIBRARY blQTKit)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(blc_image DEFAULT_MSG  BL_QTKIT_LIBRARY BL_QTKIT_INCLUDE_DIR)

mark_as_advanced(BL_QTKIT_INCLUDE_DIR BL_QTKIT_LIBRARY )

set(BL_INCLUDE_DIRS ${BL_QTKIT_INCLUDE_DIR} ${BL_INCLUDE_DIRS} )
set(BL_LIBRARIES ${BL_QTKIT_LIBRARY} ${BL_LIBRARIES} )

