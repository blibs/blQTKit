#include "blQTKit.h"

int initialized=0;
int width, height;


//This is called each time there is a new image available
static int capture_callback(blc_array *image, void *user_data){
    uint32_t format_str;
    (void)user_data; //Avoid unused warning
    
    if (!initialized) //This is executed only the first time
    {
        width=image->dims[image->dims_nb-2].length;
        height=image->dims[image->dims_nb-1].length;
        
        fprintf(stderr, "%dx%d, format:%.4s\n", width, height, UINT32_TO_STRING(format_str, image->format));
        eprintf_escape_command("s");
        initialized=1;
        blc_fprint_color_scale(stderr);

    }
    
    switch (image->format){
        case 'yuv2':
        //    if (initialized==2) {
      //          blc_eprint_cursor_up(32);//The first time we do not need to go up
               // @TODO This is a macro with bug and need to be in { }
        //    }
          //  else initialized=2;
            if (initialized==2) {
                eprintf_escape_command(MOVE_BEGIN_PREVIOUS_N_LINES, 31);
            }
            else initialized=2;
            
           
            //This function will be simplified in the future
            fprint_3Dmatrix(stderr, &image->mem, 1, 2, 1, width/32*2, 32, height/32*width*2, 32, 1);
            break;
        default:
            break;
    }
    
    //You can use the data in image->chars as you want
    //You may save it as png. ( Require blc_image )
    //You may copy it in a blc_channel. To display the image with f_view_channel (Require blc_channel)

    return 1;
}

int main(){
    
    fprintf(stderr, "Test blQTKit ( Quick Time camera) \n Ctrl+C to quit\n");
    //This allow will allow to read the keyboard without blocking
    init_capture(capture_callback, NULL); //This blocks until calling capture_callback with each new image until it return 0
    return 0;
}
